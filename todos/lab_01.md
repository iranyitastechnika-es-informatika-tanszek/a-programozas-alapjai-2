- Csekkolni, hogy milyen fejlesztői környezet van a gépeken (CodeBlock, VSCode, ...) és aszerint frissíteni az első labor doksiját.

- Csekkolni az MSYS2 URCT64 command-jait, hogy megoldható-e rendesen a feladat.

- VSCode-ot használjanak, DE: Kötelező command line-ból fordítani.

- Előadások anyagait CPPFTW-ről tanulják.

- Ha hibát találnak az oldalunkon, azt felénk jelezzék.

- Használjuk ezeket a CMake-es verziókat: https://github.com/bodand/prog2-lab-beta
