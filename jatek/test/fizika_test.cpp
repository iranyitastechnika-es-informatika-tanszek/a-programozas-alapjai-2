#include <gtest/gtest.h>

#include "fizika.h"

TEST(FizikaTest, MozgasiEnergia) {
    EXPECT_EQ(mozgasi_energia(2.0, 2.0), 4.0);
}
