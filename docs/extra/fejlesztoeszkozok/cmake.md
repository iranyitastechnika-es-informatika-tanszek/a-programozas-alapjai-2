# CMake

A programozás alapjai 1 keretein belül már volt szó arról, hogy miképp néz ki egy [többmodulos projekt fordítása](https://infoc.eet.bme.hu/tobbmodul/#2).
A fordítási és linkelési lépések kézzel történő végrehajtása már egy kisebb projekt esetében is meglehetősen körülményes tud lenni.

```bash
g++ -c fizika.cpp -o fizika.o
g++ -c megjelenites.cpp -o megjelenites.o
g++ -c main.cpp -o main.o
g++ fizika.o megjelenites.o main.o -o jatek
```

## Build szkript

Első ötletünk az lehet, hogy írjunk egy szkriptet, ami a fent említett folyamatot elvégzi helyettünk.

```bash title="build.sh"
#!/bin/bash

g++ -c fizika.cpp -o fizika.o
g++ -c megjelenites.cpp -o megjelenites.o
g++ -c main.cpp -o main.o
g++ fizika.o megjelenites.o main.o -o jatek
```

!!! info inline end ""

    A szkript futtathatóvá tételéhez ki kell adni a `chmod +x build.sh` parancsot.

Amit aztán parancssorból futtatunk:

```bash
./build.sh
```

Hosszú távon azonban ez nem egy fenttartható megoldás.
Gondoljuk végig, hogy hány helyen kéne módosítani a szkriptünket a következők megvalósításához:

- Be szeretnénk kapcsolni a következő figyelmeztetéseket: `-Werror -Wall -Wextra -Wpedantic -Wconversion`.
- A projektünkhöz új modult szeretnénk hozzáadni.
- Debugolni szeretnénk, úgyhogy be szeretnénk kapcsolni a debug információkat.
- Másik fordítót szeretnénk használni.
- Egy külső könyvtárat szeretnénk használni.

Ezek mind olyan problémák, amiket vagy sok sor módosítással tudunk elérni.
Természetesen megoldás lehet, hogy átírjuk az egész szkriptünket, és változókat, illetve elágazásokat vezetünk be.
Ezen a ponton azonban a szkriptünk már egyre jobban hasonlít egy összetett programra mint egy egyszerű fordítást végző szkriptre.

## Make

A make egy olyan program, amivel a fent említett problémákat viszonylag könnyen megoldhatjuk.
Konfigurálásához egy `Makefile` nevű fájlt kell írnunk, ami tartalmazza a fordítási folyamatot.
Az alábbi példában például már csak egy helyen kell belenyúlnunk, ha be szeretnénk kapcsolni a figyelmeztetéseket.

```Makefile title="Makefile"
CXX = g++
CXXFLAGS = -Werror -Wall -Wextra -Wpedantic -Wconversion

SRCS = fizika.cpp megjelenites.cpp main.cpp
OBJS = $(SRCS:.cpp=.o)
TARGET = jatek

all: $(TARGET)

$(TARGET): $(OBJS)
    $(CXX) $(CXXFLAGS) $^ -o $@

%.o: %.cpp
    $(CXX) $(CXXFLAGS) -c $< -o $@
```

Ezután a következő paranccsal fordíthatjuk a programunkat:

```bash
make all
```

Ez a Makefile már sokkal jobban kezelhető, mint a szkriptünk.
És olyan problémákat is megold, amik elsőre eszébe sem jutnának az embernek.
Függőségi gráfot épít a háttérben és kizárólag azokat a fájlokat fordítja újra, amik változtak.
Tehát, például ha a `fizika.cpp` fájl változik, akkor csak azt fordítja újra, és nem az összes többi fájlt.
Majd mivel a `fizika.o` fájl változott, ezért újra elvégzi a linkelést is, hogy előállítsa a `jatek` binárist.
Ezzel nagyobb projektek esetében elképesztően sok időt és erőforrást tudunk megtakarítani, hiszen amikor valaki dolgozik, akkor két fordítás és futtatás között általában csak egy-két fájlt módosít, és nem az összeset.

## CMake

Az informatikában mindig létezik egy következő absztrakciós szint.
Eddig egy `Makefile`-t írtunk, ami alapján a make automatizálta a fordítási folyamatot.
De mi lenne, ha a `Makefile` írását is automatizálnánk?
Itt jön be a képbe a [CMake](https://cmake.org/).

Miért van erre egyáltalán szükség?
A [Make](https://www.gnu.org/software/make/) nem az egyetlen eszköz, amivel fordítási folyamatot automatizálhatunk.
Léteznek más alternatívák is, mint például a [Ninja](https://ninja-build.org/).
De ott van például a Visual Studio is, ami egy [teljesen más rendszerrel](https://learn.microsoft.cppom/en-us/cpp/build/projects-and-build-systems-cpp) dolgozik.

A CMake a Makehez hasonlóan egy konfigurációs fájlt vár, ami tartalmazza a fordítási folyamatot.
Ennek a neve minden esetben `CMakeLists.txt`.
A korábbi példánkat átültetve ide így néz ki:

```cmake title="CMakeLists.txt"
cmake_minimum_required(VERSION 3.23) # (1)!
project(jatek VERSION 0.1.0 LANGUAGES CXX) # (2)!

set(CMAKE_CXX_STANDARD 17) # (3)!
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME} # (4)!
    PRIVATE 
        src/fizika.cpp # (5)!
        src/megjelenites.cpp
        src/main.cpp
)
target_compile_options(${PROJECT_NAME} # (6)!
    PRIVATE
        -Werror # (7)!
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
```

1.  A [cmake_minimum_required](https://cmake.org/cmake/help/latest/command/cmake_minimum_required.html)-nek mindenképpen kell szerepelnie az első sorban.
    Lehetőleg ne használj **3.23** előtti verziót!
    Ha nem tudod, hogy mire állítsad, akkor egy biztos kiindulási pont lehet ha futtatod a `cmake --version` parancsot, és azt a verziót adod meg amit onnan kaptál.
2.  A verzió és a használt nyelvek megadása opcionális.
    Részletek: [itt](https://cmake.org/cmake/help/latest/command/project.html).
3.  Ez a két sor elhagyható, de érdemes explicitnek lenni.
4.  Régen még az `add_executable`-nek kellett megadni a forrásfájlokat is.
    Ez még mindig működik, de a `target_sources` használata sokkal átláthatóbb.
5.  A forrásfájlokat célszerű kiszervezni egy **src** mappába.
    Itt feltételezem, hogy így van szervezve a projekt.
    Ha nem így lenne, akkor elég lenne simán **fizika.cpp**-t írni.
6.  Ezek csak a fordításkor használt kapcsolók (amikor **\*.cpp**-ből **\*.o** lesz).
    A linkelésnél használt kapcsolókat a `target_link_options`-nel lehet megadni.
7.  Ez az öt kapcsoló nem véletlen szerepel itt példaként.
    Használatuk erősen ajánlott!

Vegyük észre, hogy milyen egyszerűvé vált így a konfiguráció.
A sorok magukért beszélnek, hogy mit csinálnak:

- Megadjuk, hogy legalább milyen CMake verzióra van szükségünk.
- Megadjuk a projekt nevét.
- Megadjuk, hogy milyen C++ szabványt szeretnénk használni.
- Megadjuk, hogy egy futtatható állományt szeretnénk létrehozni, aminek a neve egyezzen meg a projekt nevével.
- Megadjuk, hogy milyen forrásfájlok tartoznak az előbb létrehozott futtatható állományhoz.
- Végül pedig megadjuk, hogy milyen fordítási kapcsolókat szeretnénk használni ezen futtatható állományon.

Hogyan kell ezután fordítani CMake segítségével?

### Konfigurálás

Először egy konfigurálásra van szükség.
Ezt pár [parancssori kapcsoló](https://cmake.org/cmake/help/latest/manual/cmake.1.html#options) megadásával érhetjük el:

```bash
cmake -B build
```

-   !!! note inline end ""

        Git használata esetén adjuk hozzá a `.gitignore` fájlhoz a `build` mappát.

    Itt kell megmondani a CMake-nek a `-B` kapcsoló segítségével, hogy hova szeretnénk a konfigurációs fájlokat elhelyezni.
    Ezt követően a CMake létrehozza a megadott mappát, és ebbe helyezi el a konfigurációs fájlokat.
    Ha nem adjuk meg ezt a kapcsolót, akkor a CMake a jelenlegi mappába fogja elhelyezni a konfigurációs fájlokat - ezt lehetőleg kerüljük.

-   Fontos, hogy a parancs kiadásakor ugyanabban a mappában legyünk, ahol a **CMakeLists.txt** fájl található.
    Ha máshol állunk, akkor a `-S` kapcsolóval megadhatjuk, hogy hol található a **CMakeLists.txt** fájl.

-   Amennyiben pontosan meg szeretnénk mondani, hogy milyen build rendszert szeretnénk használni, akkor a `-G` kapcsolóval tehetjük meg.
    Például ha Ninja-t szeretnénk használni (mert az sokkal gyorsabb mint a Make), akkor a `-G Ninja` kapcsolót kell megadnunk.
    Ennek hiányában a CMake a rendszer alapértelmezett build rendszerét fogja használni.

-   Meg lehet adni még azt is, hogy [milyen módban fordítson a CMake](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html).
    Például ha a `Release` módban szeretnénk fordítani, akkor a `-DCMAKE_BUILD_TYPE=Release` kapcsolót kell megadnunk.
    Fejlesztés során érdemes a `Debug` módot használni, mert az tartalmazza a debug információkat.

-   Ha szeretnénk, akkor meg lehet mondani azt is, hogy melyik fordítót szeretnénk használni.
    Például ha a [Clang](https://clang.llvm.org/) fordítót szeretnénk használni, akkor a `-DCMAKE_CXX_COMPILER=clang++` kapcsolót kell megadnunk.
    Ennek hiányában a CMake itt is az alapértelmezettet választja.

-   Érdemes még megemlíteni a `-DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE` kapcsolót.
    Ez arra jó, hogy a CMake kigenerálja a `compile_commands.json` fájlt, amit sok fejlesztést segítő eszköz használ.
    Ilyen például a [Clang-Tidy](https://clang.llvm.org/extra/clang-tidy/), vagy a [Clang-Format](https://clang.llvm.org/docs/ClangFormat.html).
    De érdemes megemlíteni a [SonarLint](https://www.sonarsource.com/products/sonarlint/)-et is.
    Ezeknek használata nem nélkülözhetetlen, de nagyban megkönnyítik a fejlesztést.

A fenti kapcsolókat használva tehát így is kinézhet egy konfigurálás:

```bash
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE -DCMAKE_CXX_COMPILER=clang++ -S . -B build -G Ninja
```

### Fordítás

A projekt tényleges fordításához a következő parancsot kell kiadnunk:

```bash
cmake --build build
```

A `--build` kapcsoló azért kell, hogy megmondjuk a CMake-nek, hogy hol van az a mappa ahol a konfigurációs fájlok találhatóak.

### Futtatás

Az elkészült programunkat a `build` mappában találjuk.
Futtatni a következő paranccsal tudjuk:

```bash
build/jatek
```
