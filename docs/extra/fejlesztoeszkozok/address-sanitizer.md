# AddressSanitizer

A programokban előforduló [sérülékenységek leggyakoribb okozói](https://cwe.mitre.org/top25/archive/2023/2023_stubborn_weaknesses.html) között a memóriakorrupcióból eredő problémák mindig előkelő helyen szerepelnek.

Csak, hogy pár példát említsünk:

- Túlindexelés
- Felszabadítás utáni használat
- NULL pointer dereferálása

Ezek elkerülésére különösen fontos (lenne) odafigyelni.

## Kezdetleges eszközök

Elsőre jó ötletnek tűnhet, hogy saját magunk írjunk tesztelő keretrendszereket.
Ilyen volt prog1-ből a [debugmalloc](https://infoc.eet.bme.hu/debugmalloc/), vagy ilyen most prog2-ből a memtrace.
Ezeknek a használata azonban macerás, hiszen minden minden forrásfájlba be kell szerkeszteni valamit, hogy működjön, amit úgyanúgy el lehet felejteni, mint egy sima `delete`-et.
Arról már ne is beszéljünk, hogy a memtrace például nem működik a C++11-es `delete` kulcsszó új fajta jelentésével.

## Rendes megoldások

### AddressSanitizer

Az [AddressSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) egy olyan eszköz, ami futási időben ellenőrzi a memóriahasználatot.
Képes észlelni a túlindexelést, a felszabadítás utáni használatot, a NULL pointer dereferálást, és még sok más hibát is.

Használata nagyon kényelmes, mert egyáltalán nem szükséges hozzá módosítani a forrásfájlokat.
Ha elolvassuk a dokumentációt, kiderül, hogy elég csak fordításkor (és linkeléskor) megadni a `-fsanitize=address` kapcsolót, és már kész is vagyunk.

Hogy tud ez így működni mindenféle hozzáadandó fájl nélkül?
A válasz az, hogy a modern fordítók (pl. GCC, Clang) már beépítve tartalmazzák az AddressSanitizer-t.

Van még egy másik kapcsoló is amit érdemes megadni, ez a `-fno-omit-frame-pointer`.
Így hiba esetén sokkal szebb stack trace-t kapunk, magát a hiba okát is könnyebben meg fogjuk tudni találni.

Ahhoz, hogy a [korábbi CMake projektünket](./cmake.md/#cmake_1) AddressSanitizer-rel használhassuk, adjuk hozzá a **CMakeLists.txt**-ben a `target_compile_options`-höz a `-fsanitize=address` és a `-fno-omit-frame-pointer` kapcsolókat:

```cmake title="CMakeLists.txt" hl_lines="22-30"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
        src/main.cpp
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -fsanitize=address
        -fno-omit-frame-pointer
)
target_link_options(${PROJECT_NAME}
    PRIVATE
        -fsanitize=address
)
```

Fontos kiemelni, hogy mint minden ellenőrzési technika, az AddressSanitizer is jár futási idejű költséggel.
A kódunk átlagban nagyjából 50-100%-al lassabb lesz tőle.
Ezért azt szeretnénk, hogy csak a fejlesztés során használjuk, a végleges verzióban már ne legyen benne.
Ezt CMake használata esetén például úgy érhetjük el, hogy ha csak a **Debug** konfiguráció esetén használjuk a kapcsolókat:

```cmake title="CMakeLists.txt" hl_lines="22 32"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
        src/main.cpp
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options(${PROJECT_NAME}
        PRIVATE
            -fsanitize=address
            -fno-omit-frame-pointer
    )
    target_link_options(${PROJECT_NAME}
        PRIVATE
            -fsanitize=address
    )
endif()
```

### Valgrind

A [Valgrind](https://valgrind.org/docs/manual/quick-start.html) szintén egy olyan eszköz amely segít felismerni számos memóriakezelési hibát.
Részletesebben most nem fogunk vele foglalkozni, de az érdeklődőknek érdemes lehet utánanézni.
