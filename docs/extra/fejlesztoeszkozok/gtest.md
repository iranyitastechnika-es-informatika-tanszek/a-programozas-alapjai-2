# GTest

A programokat nem lehet tökéletesen megírni.
Hiába kapcsoljuk be fordítás során a figyelmeztetéseket, és hiába használunk statikus analízist, ezek csak arra tudják felhívni a figyelmünket, hogy valami nem stimmel a kódunkban.
Abból, hogy nem szólnak semmire, még nem következik, hogy a programunk hibátlan.
A lényeg, hogy bármennyire is igyekszünk, a programjainkba jó eséllyel hibák fognak kerülni.
A célunk, hogy ezeket a hibákat minél hamarabb észrevegyük, és kijavítsuk.
Erre ad egy jó módszert a tesztelés.
A [tesztelésnek több fajtája is létezik](https://microsoft.github.io/code-with-engineering-playbook/automated-testing/e2e-testing/testing-comparison/), mi most a unit-, azaz egységtesztelésre fogunk koncentrálni.

!!! note "Mire jók még a tesztek?"

    Amikor az ember kódot ír a nulláról, akkor azt általában addig rugdossa, amíg nem működik.
    Sokszor előfordul azonban, hogy a kód működik, de mi mégis szeretnénk valamit változtatni rajta.
    Például lehet, hogy azért szeretnénk refaktorálni a belső implementációját egy függvénynek, hogy az gyorsabb legyen.
    Ebben az esetben a következőket érdemes követni:
    
    1.  Írjunk teszteket a meglévő, már működő kódhoz.
    2.  Refaktoráljuk a kódot.
    3.  Győződjünk meg róla, hogy a tesztek továbbra is sikeresen lefutnak.

Eddig amikor a programunkat fordítottuk, akkor mindig egy futtatható állomány lett a végeredmény.
Mostantól kezdve viszont két futtatható állományt fogunk készíteni.
Az egyik továbbra is a programunk lesz, változatlanul.
A másik pedig a megírt osztályokat és függvényeket fogja tesztelni.

Természetesen a teszteket is nekünk kell megírni egyesével.
Az azonban nagyon nem mindegy, hogy milyen tesztelő keretrendszert használunk.
Lehet használni egy házi készítésű tesztelő keretrendszert, de lehet használni egy ipari szabványt is.
Az utóbbi esetben sokkal könnyebb dolgunk lesz a tesztek írásával, futtatásával és karbantartásával is.

## GoogleTest

C++-ban az egyik legelterjedtebb tesztelő keretrendszer a [GoogleTest](https://google.github.io/googletest/).
A tesztek írását és futtatását nagyon egyszerűvé teszi, és a tesztek futtatásának eredménye is sokkal szebb és átláthatóbb, mint egy házi készítésű tesztelő keretrendszer esetén.

### Teszt írása

Tegyük fel, hogy az alábbi függvényünket szeretnénk tesztelni:

```cpp title="src/fizika.h"
#pragma once

double mozgasi_energia(double m, double v);
```

A GoogleTest tesztek írásához a `TEST` makrót kell használni.
Ez a makró két paramétert vár: az első a teszt suit neve, a második pedig a teszt neve.
A teszt suit csak egy csoportosítás, úgy kell rá tekinteni, mint egy mappára.
A lényeg, hogy bármilyen nevet adhatunk neki, de érdemes olyan nevet választani, ami jól leírja, hogy a tesztek milyen függvényt vagy osztályt tesztelnek.

Az egyes tesztekben az `EXPECT_EQ` makróval lehet összehasonlítani a két értéket.
Természetesen a GoogleTest többféle összehasonlító makrót is tartalmaz, ezekről bővebben a [dokumentációban](https://google.github.io/googletest/reference/assertions.html) lehet olvasni.

```cpp title="test/fizika_test.cpp"
#include <gtest/gtest.h>

#include "fizika.h"

TEST(FizikaTest, MozgasiEnergia) {
    EXPECT_EQ(mozgasi_energia(2.0, 2.0), 4.0);
}
```

És igazából készen is vagyunk.
Vegyük észre, hogy nem kellett semmilyen `main` függvényt írnunk, a GoogleTest ezt helyettünk megteszi.
Most már csak annyi van hátra, hogy valahogy működésre bírjuk a teszteket.

!!! danger "Vigyázat!"

    Egy teszt írása a valóságban még közel sem elegendő!
    Most csak a példa kedvéért nem bonyolítjuk túl a dolgokat.

### CMake függőség hozzáadása

A GoogleTest-et [hozzá kell adni a CMake projektünkhöz](https://google.github.io/googletest/quickstart-cmake.html).
CMake-ben a [függőség hozzáadásának](https://cmake.org/cmake/help/latest/guide/using-dependencies/index.html) több módja is létezik.

=== "FetchContent"

    A legegyszerűbb módszer a [FetchContent](https://cmake.org/cmake/help/latest/module/FetchContent.html) használata.

    ```cmake title="CMakeLists.txt" hl_lines="7-13"
    cmake_minimum_required(VERSION 3.23)
    project(jatek VERSION 0.1.0 LANGUAGES CXX)

    set(CMAKE_CXX_STANDARD 17)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)

    include(FetchContent)
    FetchContent_Declare(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        v1.14.0
    )
    FetchContent_MakeAvailable(googletest)

    add_executable(${PROJECT_NAME})
    target_sources(${PROJECT_NAME}
        PRIVATE 
            src/fizika.cpp
            src/megjelenites.cpp
            src/main.cpp
    )
    target_compile_options(${PROJECT_NAME}
        PRIVATE
            -Werror
            -Wall
            -Wextra
            -Wpedantic
            -Wconversion
    )
    ```

    Ez a projektünk konfigurálásakor letölti a GoogleTest-et a `build/_deps` mappába.
    Ennek megfelelően amikor a projektünket először fordítjuk, akkor nekünk kell fordítani a GoogleTest-et is.

=== "find_package"

    A második módszer a [find_package](https://cmake.org/cmake/help/latest/command/find_package.html) használata.

    ```cmake title="CMakeLists.txt" hl_lines="7"
    cmake_minimum_required(VERSION 3.23)
    project(jatek VERSION 0.1.0 LANGUAGES CXX)

    set(CMAKE_CXX_STANDARD 17)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)

    find_package(GTest REQUIRED)

    add_executable(${PROJECT_NAME})
    target_sources(${PROJECT_NAME}
        PRIVATE 
            src/fizika.cpp
            src/megjelenites.cpp
            src/main.cpp
    )
    target_compile_options(${PROJECT_NAME}
        PRIVATE
            -Werror
            -Wall
            -Wextra
            -Wpedantic
            -Wconversion
    )
    ```

    Ebben az esetben előkövetelmény, hogy a GoogleTest telepítve legyen a rendszerünkre. (1)
    Cserébe viszont nem nekünk kell fordítani a GoogleTest-et, hanem a CMake automatikusan megtalálja a telepített (és fordított) verziót.
    { .annotate }

    1.  Például Arch Linux / MSYS2 alatt a `sudo pacman -S gtest` paranccsal lehet telepíteni.
        
        - Debian/Ubuntu: `sudo apt install libgtest-dev`
        - Fedora: `sudo dnf install gtest-devel`

        Vagy érdemes egy C++ package managert használni, mint a [vcpkg](https://vcpkg.io/en/) vagy a [Conan](https://conan.io/).

### Futtatható állomány létrehozása

Amennyiben sikerült valahogy hozzáadni a GoogleTest-et a projektünkhöz, akkor már fel is lehet használni ahhoz, hogy létrehozzunk egy futtatható állományt a tesztek futtatásához.

```cmake title="CMakeLists.txt" hl_lines="25-45"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(GTest REQUIRED)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
        src/main.cpp
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)

add_executable(${PROJECT_NAME}_test)
target_sources(${PROJECT_NAME}_test
    PRIVATE
        src/fizika.cpp
        test/fizika_test.cpp
)
target_include_directories(${PROJECT_NAME}
    PRIVATE
        src # (1)!
)
target_compile_options(${PROJECT_NAME}_test
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        GTest::gtest_main
)
```

1.  Ez azért kell, hogy a **fizika_test.cpp** fájlban azt írhassuk, hogy `#include "fizika.h"`.
    Enélkül `#include "../src/fizika.h"`-t kéne írni.

Ezután ugyanúgy kell konfigurálni(1) és fordítani(2) a projektet, mint eddig.
A futtatás pedig szintén gyerekjáték:
{ .annotate }

1.  Konfigurálás:
    ```bash
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE -DCMAKE_CXX_COMPILER=clang++ -S . -B build -G Ninja
    ```

2.  Fordítás:
    ```bash
    cmake --build build
    ```

```bash
build/jatek_test
```

### CTest integráció

Ezen a ponton akár meg is állhatunk, mert a tesztek futtatása már működik.
Később azonban nagyon jól fog még jönni, ha működésre bírjuk a CTest-et is.

```cmake title="CMakeLists.txt" hl_lines="25 49-50"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(GTest REQUIRED)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
        src/main.cpp
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)

enable_testing() # (1)!

add_executable(${PROJECT_NAME}_test)
target_sources(${PROJECT_NAME}_test
    PRIVATE
        src/fizika.cpp
        test/fizika_test.cpp
)
target_include_directories(${PROJECT_NAME}
    PRIVATE
        src
)
target_compile_options(${PROJECT_NAME}_test
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        GTest::gtest_main
)

include(GoogleTest) # (2)!
gtest_discover_tests(${PROJECT_NAME}_test) # (3)!
```

1.  Ez azért kell, hogy bekapcsoljuk a projekten a tesztelést.
    Ha ezt nem tesszük meg, akkor a `gtest_discover_tests` nem fog működni.
2.  Ez húzza be és teszi eléhetővé a `gtest_discover_tests` függvényt.
3.  Ez az ami kigenerálja a build mappába a **CTestTestfile.cmake** fájlt, ami tartalmazza a CTest számára szükséges információkat a tesztek futtatásához.

De mi is pontosan a CTest?
A CTest egy olyan eszköz, ami a CMake-kel együtt települ a gépünkre.
Arra való, hogy teszteket futtasson, és azok eredményeit szépen formázva megjelenítse.
De lehetőséget ad arra is, hogy csak bizonyos teszteket futtassunk.
Például ha csak a korábban írt `MozgasiEnergia` tesztet szeretnénk futtatni, akkor a következő parancsot kell kiadnunk:

```bash
cd build
ctest -R ^FizikaTest\.MozgasiEnergia$ # (1)!
cd ..
```

1.  Az `-R` kapcsolóval lehet megadni, hogy mely teszteket szeretnénk futtatni.
    Amelyik teszt neve illeszkedik a megadott reguláris kifejezésre, azt fogja a CTest futtatni.
    A `^` és a `$` karakterek a reguláris kifejezések kezdő és záró karakterei.
    A `\` azért kell, hogy a `.`-ot karakterként értelmezze a CTest, mert különben a `.` a reguláris kifejezésekben bármilyen karaktert jelent.
    Így a `^FizikaTest\.MozgasiEnergia$` azt jelenti, hogy a teszt neve a `FizikaTest.MozgasiEnergia` legyen.

Amiért pedig nagyon-nagyon hasznos a CTest, az az, hogy sok fejlesztőkörnyezet (például a Visual Studio Code) támogatja.
Így a tesztek gombnyomásra futtathatóak lesznek, és az eredmények is szépen megjelennek:

![CTest Visual Studio Code-ban](ctest_vscode.png)

### AddressSanitizer

A tesztelés során szeretnénk felfedezni az esetleges memóriakezelési hibákat is, úgyhogy következő lépésként nézzük meg, hogy miképp lehet integrálni a korábban megismert AddressSanitizert a tesztekbe.
Mielőtt azomban ebbe belefognánk, szervezzük át a projektünket egy kicsit!

Az eddigiekben a `src` mappában voltak a forrásfájljaink, és a `test` mappában a tesztek.
Hozzunk létre egy `include` mappát, ahova átmozgatjuk a header fájljainkat.
Ezt általában akkor szoktuk megtenni, ha programkönyvtárat ítunk és mások is szeretnék használni a kódunkat.
Eddig fordításkor két különálló futtatható állományt készítettünk, az egyik a programot, a másik a teszteket futtatta.
Most hozzunk létre egy library-t, amit majd linkelünk a programhoz és a tesztekhez is, így lényegében mi leszünk a felhasználói is a készített library-nek.

Lehet, hogy ez így hirtelen bonyolultnak hangzik, de a gyakorlatban nagyon hasznosnak fogjuk találni, ugyanis az eddigiekben a CMakeLists.txt fájlunkban minden forrásfájl kétszer szerepelt, egyszer a programhoz, egyszer a tesztekhez.
Ezentúl azonban csak egyszer kell majd felsorolni a forrásfájljainkat, és a CMake majd megoldja, hogy a megfelelő futtatható állományokat készítse.
A kódunk szintjén pedig az új mappa bevezetésén kívül semmit sem kell változtatni, csak a CMake-nek kell megmondani, hogy máshogy fordítson.

```cmake title="CMakeLists.txt" hl_lines="9-18 28-32 41-44 48-52 61-65"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(GTest REQUIRED)

add_library(${PROJECT_NAME}_lib)
target_include_directories(${PROJECT_NAME}_lib
    PUBLIC
        include
)
target_sources(${PROJECT_NAME}_lib
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
)
target_compile_options(${PROJECT_NAME}_lib
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/main.cpp
)
target_compile_options(${PROJECT_NAME}
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
target_link_libraries(${PROJECT_NAME}
    PRIVATE
        ${PROJECT_NAME}_lib
)

enable_testing() 

add_executable(${PROJECT_NAME}_test)
target_sources(${PROJECT_NAME}_test
    PRIVATE
        test/fizika_test.cpp
)
target_compile_options(${PROJECT_NAME}_test
    PRIVATE
        -Werror
        -Wall
        -Wextra
        -Wpedantic
        -Wconversion
)
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        ${PROJECT_NAME}_lib
        GTest::gtest_main
)

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME}_test)
```

Habár megszabadultunk attól a redundanciától, hogy kétszer kelljen felsorolni a forrásfájljainkat, de azért még mindig van egy kis redundancia a CMakeLists.txt fájlunkban, ugyanis, a használni kívánt warning flag-eket többször is meg kell adnunk.
Erre is létezik azonban megoldás, egy CMake függvény-t fogunk írni, ami a megadott target-re beállítja a warning flag-eket.

```cmake title="CMakeLists.txt" hl_lines="9-18 30 37 50"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(GTest REQUIRED)

function(add_compile_options target)
    target_compile_options(${target}
        PRIVATE
            -Werror
            -Wall
            -Wextra
            -Wpedantic
            -Wconversion
    )
endfunction()

add_library(${PROJECT_NAME}_lib)
target_include_directories(${PROJECT_NAME}_lib
    PUBLIC
        include
)
target_sources(${PROJECT_NAME}_lib
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
)
add_compile_options(${PROJECT_NAME}_lib)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/main.cpp
)
add_compile_options(${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME}
    PRIVATE
        ${PROJECT_NAME}_lib
)

enable_testing() 

add_executable(${PROJECT_NAME}_test)
target_sources(${PROJECT_NAME}_test
    PRIVATE
        test/fizika_test.cpp
)
add_compile_options(${PROJECT_NAME}_test)
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        ${PROJECT_NAME}_lib
        GTest::gtest_main
)

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME}_test)
```

Ezen a ponton el is készültünk a CMakeLists.txt fájlunk átszervezésével, most már gyerekjáték hozzáadni az AddressSanitizert.

```cmake title="CMakeLists.txt" hl_lines="27-37"
cmake_minimum_required(VERSION 3.23)
project(jatek VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(GTest QUIET)
if (NOT GTest_FOUND)
    include(FetchContent)
    FetchContent_Declare(
            googletest
            GIT_REPOSITORY https://github.com/google/googletest.git
            GIT_TAG v1.14.0
    )
    FetchContent_MakeAvailable(googletest)
endif ()

function(add_compile_options target)
    target_compile_options(${target}
        PRIVATE
            -Werror
            -Wall
            -Wextra
            -Wpedantic
            -Wconversion
    )
    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        target_compile_options(${target}
            PRIVATE
                -fsanitize=address
                -fno-omit-frame-pointer
        )
        target_link_options(${target}
            PRIVATE
                -fsanitize=address
        )
    endif()
endfunction()

add_library(${PROJECT_NAME}_lib)
target_include_directories(${PROJECT_NAME}_lib
    PUBLIC
        include
)
target_sources(${PROJECT_NAME}_lib
    PRIVATE 
        src/fizika.cpp
        src/megjelenites.cpp
)
add_compile_options(${PROJECT_NAME}_lib)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
    PRIVATE 
        src/main.cpp
)
add_compile_options(${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME}
    PRIVATE
        ${PROJECT_NAME}_lib
)

enable_testing() 

add_executable(${PROJECT_NAME}_test)
target_sources(${PROJECT_NAME}_test
    PRIVATE
        test/fizika_test.cpp
)
add_compile_options(${PROJECT_NAME}_test)
target_link_libraries(${PROJECT_NAME}_test
    PRIVATE
        ${PROJECT_NAME}_lib
        GTest::gtest_main
)

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME}_test)
```

## Catch2

A [Catch2](https://github.com/catchorg/Catch2) egy másik nagyon népszerű tesztelő keretrendszer C++-ban.
Ezt is bátran lehet használni, ha a GoogleTest nem tetszik valamiért.
