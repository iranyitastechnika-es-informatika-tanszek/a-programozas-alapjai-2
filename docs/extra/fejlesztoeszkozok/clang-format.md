# Clang-Format

A kódformázás egy olyan témakör, amit sokan nem vesznek elég komolyan.
Pedig egy jól formázott kód sokkal könnyebben olvasható, és így sokkal könnyebben karbantartható is.
Márpedig a kódot sokkal többször olvassuk, mint írjuk.
Ez azért is különösen fontos, hiszen nagyon sokszor van, hogy nem is mi, hanem valaki más olvassa a kódunkat.
Az ő dolgát könnyítjük meg azáltal, hogy a kódunkat jól formázzuk.

Ez az írás nem arról fog szólni, hogy milyen formázási szabályokat érdemes követni.
Teljesen mindegy, hogy Java-s vagy C#-os világban megszokott módon nevezzük el a függvényeinket és tördeljük a kódunkat.

```java
public void fuggveny() {
    // Java kód
}
```

```csharp
public void Fuggveny()
{
    // C# kód
}
```

A lényeg, hogy akkor olvasható egy kód könnyedén, ha a helyes indentálás mellett egységes formázási szabályokat is követünk.

## Automatizált formázás

Az automatizált formázás azt jelenti, hogy a kódunkat egy program automatikusan formázza.
A legtöbb fejlesztőkörnyezet alapból támogat efféle funkciót.
Használatuk azért nagyon ajánlott, mert így nem kell manuális formázással időt tölteni, és véletlen sem fordulhat elő, hogy a megszokottól eltérően formázzuk a kódunkat.

## Clang-Format

Modern programnyelvek esetén gyakran van, hogy a nyelvhez tartozik saját ajánlott névkonvenció és formázási stílus.
Ilyen például [Rust](https://www.rust-lang.org/) esetében a [Rustfmt](https://rust-lang.github.io/rustfmt/).

C++ esetében nincs ilyen beépített formázó, de szerencsére van egy nagyon jó harmadik féltől származó - a [Clang-Format](https://clang.llvm.org/docs/ClangFormat.html).
Most ennek a használatáról lesz szó.

### Telepítés

Ha már van Clang fordítónk, akkor azzal együtt elvileg a Clang-Format is telepítve van.

### Használat

A Clang-Formatot a következő módon tudjuk használni:

```bash
clang-format src/main.cpp
```

Ez a parancs a `src/main.cpp` fájlt formázza.
Pontosabban a formázott kódot kiírja a standard kimenetre.
Ha a formázást helyben szeretnénk végezni (mint szinte mindig), akkor a `-i` kapcsolót kell megadni:

```bash
clang-format -i src/main.cpp
```

### Konfiguráció

Elég valószínű, hogy a Clang-Format által használt alapértelmezett konfiguráció nem pont az ízlésünknek megfelelő eredményt adja.
Mint korábban említettem, C++ esetében nincsen egy egységesen elfogadott formázási konvenció.
Így az a szokás, hogy minden projektnek van saját formázási konvenciója.
Ennek leírását a projekt gyökerében, a **.clang-format** nevű fájlban tároljuk.

A Clang-Format alapból megpróbálja megtalálni ezt a **.clang-format** fájlt.
Ha sehol sem találja, akkor az alapértelmezett konfigurációt használja.
Először készítsünk egy alap konfigurációt.
Ezt a következő paranccsal tehetjük meg:

```bash
clang-format -dump-config > .clang-format # (1)!
```

1.  Alapból elég nehézkes tud lenni a **.clang-format**-ban minden szabály beállítása, úgyhogy érdemes lehet [egy jól ismert stílus](https://github.com/microsoft/clang/blob/master/docs/ClangFormatStyleOptions.rst#configurable-format-style-options) használata kiindulási alapként.
    Ilyen például az [LLVM Coding Standards](https://llvm.org/docs/CodingStandards.html) vagy a [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html).

    ```bash
    clang-format -style=llvm -dump-config > .clang-format
    ```

Ez a parancs a standard kimenetre kiírja a Clang-Format által használt konfigurációt.
A **>** karakterrel pedig a kimenetet a **.clang-format** fájlba irányítjuk.
Ezután a **.clang-format** fájlt a saját igényeinknek megfelelően szerkeszthetjük.

Az összes lehetséges beállításról a [Clang-Format dokumentációjában](https://clang.llvm.org/docs/ClangFormatStyleOptions.html) olvashatunk.
Kiindulásnak azért itt hagyok pár példát:

-   Ha nem tetszik az alapértelmezett 2 karakteres indentálás, akkor a **.clang-format** fájlban az `IndentWidth` értékét módosíthatjuk:

    ```yaml
    AccessModifierOffset: -4 # (1)!
    IndentWidth:     4
    ```

    1.  Az `AccessModifierOffset` azt állítja be, hogy a `public`, `protected`, `private` kulcsszavakat mennyivel kell visszább tolni a függvények kezdőpontjától.
        Ha az indentálást állítjuk, akkor ezt is érdemes lehet módosítani.

-   Ha nem tetszik, ahogy a rövid függvényeket formázza, akkor az [`AllowShortFunctionsOnASingleLine`](https://clang.llvm.org/docs/ClangFormatStyleOptions.html#allowshortfunctionsonasingleline) értékét módosíthatjuk:

    === "All"

        ```yaml
        AllowShortFunctionsOnASingleLine: All
        ```

        ```cpp
        Image::Image(std::size_t width, std::size_t height) : width(width), height(height) {}

        std::size_t Image::get_width() const { return width; }
        ```

    === "Empty"

        ```yaml
        AllowShortFunctionsOnASingleLine: Empty # (1)!
        ```

        1.  Ez az én személyes kedvencem.

        ```cpp
        Image::Image(std::size_t width, std::size_t height) : width(width), height(height) {}

        std::size_t Image::get_width() const {
            return width;
        }
        ```

    === "None"

        ```yaml
        AllowShortFunctionsOnASingleLine: None
        ```

        ```cpp
        Image::Image(std::size_t width, std::size_t height) : width(width), height(height) {
        }

        std::size_t Image::get_width() const {
            return width;
        }
        ```

-   Ha nem tetszik, hogy egy sorba vannak írva a templatejeink, akkor az `AlwaysBreakTemplateDeclarations` értékét lehet állítani:

    ```yaml
    AlwaysBreakTemplateDeclarations: Yes
    ```

-   Ha úgy gondoljuk, hogy túl hamar tördeli a sorainkat, akkor a `ColumnLimit` értékét növelhetjük:

    ```yaml
    ColumnLimit: 160
    ```

-   Ha azt szeretnénk, hogy az egy soros `if`, `for`, `while` utasítások után mindig ki legyen írva a kapcsos zárójel, akkor az `InsertBraces` értékét módosíthatjuk:

    ```yaml
    InsertBraces:    true # (1)!
    ```

    1.  Szerintem ezt érdemes lehet bekapcsolni.
