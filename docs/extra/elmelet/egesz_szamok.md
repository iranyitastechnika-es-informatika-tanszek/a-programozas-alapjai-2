# Egész számok

Elsőre szokatlannak tűnhet, hogy írni kell a számokról, de sajnos C-ben és C++-ban nem olyan egyszerű a helyzet, hogy ész nélkül biztosan minden jó lesz.

Hány bites az `int` típus?
A válasz: attól függ.
A szabvány nem írja elő, hogy milyen hosszú legyen az `int` típus.
A szabvány csak azt írja elő, hogy az `int` típusnak legalább 16 bitesnek kell lennie.
A gyakorlatban azonban a legtöbb rendszeren 32 bites az `int` típus.

## Pontos méretek

Programozóként pontosan szeretnénk tudni, hogy mit csinál a kódunk.
Ebből a célból létezik C-ben/C++-ban az `<stdint.h>`/`<cstdint>` fejléc.
Ezekben a fejlécekben olyan típusok vannak, amik pontosan meghatározott hosszúságúak.
Például ha egy 32 bites, előjeles, egészre van szükségünk, akkor az `int32_t` típust használhatjuk.
Egy 8 bites előjel nélküli szám esetében pedig az `uint8_t` áll a rendelkezésünkre.

## Architektúra függőség

Sajnos a fenti gyakorlatok követéséből még nem következik, hogy minden egész számnak tudjuk a méretét.

Mekkora egy pointer mérete?
A pointer mérete a processzor architektúrájától függ.
Ez egy olyan dolog, amit a programozó nem tudhat előre, de valahogy kezelnie kell, hiszen csak két egyforma méretű egész szám között van értelme műveletet végezni, márpedig a pointerek is számok.

Hol fordul ez elő?
Gondoljuk csak az `std::vector` osztályra.
Az indexeléshez meg kell adnunk egy számot, hogy hányadik elemre szeretnénk hivatkozni.
Mi lehet az index típusa?
Implementációs szinten a `std::vector` egy tömbre mutató pointert tárol.
Az indexelő operátor pedig csak ehhez a pointerhez adja hozzá az indexet.
Ahhoz, hogy ez a művelet értelmes legyen, azaz két egyforma méretű egész szám közötti művelet legyen, az indexnek is ugyanolyan méretűnek kell lennie, mint a pointernek.
Kell nekünk tehát egy szám aminek a mérete megegyezik a pointer méretével.
Ez pedig nem más mint az `size_t` típus.

A `size_t` típus tehát az a típus ami 32 bites architektúrán 32 bites, 64 bites architektúrán 64 bites.
Ezáltal a mérete mindig megegyezik a pointerek méretével.

## Figyelmeztetések a fordítótól

Ismert gyakorlat, hogy fordítás során érdemes a lehető legtöbb figyelmeztetést bekapcsolni.
Ezek közül ismert lehet a **-Wall** kapcsoló, ami a legtöbb figyelmeztetést bekapcsolja, azonban a neve félrevezető, mert nem minden figyelmeztetést kapcsol be.
Egy másik gyakran használt kapcsoló a **-Wextra**, ami még több figyelmeztetést kapcsol be, de sajnos még nem ez az összes.

Az egész számok helyes használata szempontjából a **-Wconversion** figyelmeztetés a leghasznosabb.
Ez minden eltérő méretű vagy előjelű egész szám típus közötti műveletnél figyelmeztetést ad.
Ezekre különösen érdemes odafigyelni!
A figyelmen kívül hagyásukból különösen nehezen felderíthető hibák származhatnak.

## Összefoglalás

Ha egy egész számra van szükségünk, akkor gondoljuk végig, hogy szükséges-e, hogy előjeles legyen-e.
Ha nem, akkor használjuk az előjel nélküli változatot.

A méretet tekintve amennyiben nem tudjuk pontosan, hogy milyen méretűre van szükségünk, akkor ökölszabályként jó a 32 bites `int32_t`.
Fontos, hogy itt is konkréten szerepel a méret, ezáltal ha később változtatni kell, akkor pontosan tudni fogjuk, hogy miről mire változtattunk.
