# Tömbök

A C-ben használt tömbök nagyon hasznosak, de sok hibaforrást hordoznak magukkal.
A C++ ezeket is próbálja részben orvosolni.

## std::array

Mit csinál a következő C kód?

```c
int elements_1[3] = {1, 2, 3};
int elements_2[] = elements_1;
elements_2[0] = 4;
printf("%d\n", elements_1[0]);
```

A gyakorlott C programozók tudják, hogy ez a kód nem fordul le.
A második sorban egy tömböt szeretnénk érték szerint másolni.
Ilyet nem lehet C-ben.

A fenti példához hasonló kódot ritkán szeretnénk írni, az alábbi viszont már sokkal életszerűbb:

```c
void foo(int elements_2[], size_t size) {
    elements_2[0] = 4;
}
// ...
int elements_1[3] = {1, 2, 3};
foo(elements_1, 3);
printf("%d\n", elements_1[0]);
```

Ez a kód viszont lefordul és a kimenet **4**.
Egy sima `int` esetében nem fordulhat elő, hogy mindenféle címképző operátor nélkül megváltozik az értéke.
Ott egy másolatot kapnánk a függvényünkben.

Az `std::array` azért létezik, hogy a fentiekhez hasonlóak ne fordulhassanak elő.
Használatához szükségünk van a `<array>` fejlécre:

```cpp
#include <array>
```

Ezután már használhatjuk is:

```cpp
std::array<int, 3> elements_1 = {1, 2, 3};
std::array<int, 3> elements_2 = elements_1;
elements_2[0] = 4;
std::cout << elements_1[0] << std::endl;
```

A fenti kód lefordul.
Értékadás esetén teljes másolat készül.
Tehát a kimenet **1**.

Ha a C-hez hasonló működést szeretnénk, akkor azt referencia használatával tudjuk elérni:

```cpp
std::array<int, 3> elements_1 = {1, 2, 3};
std::array<int, 3> &elements_2 = elements_1;
elements_2[0] = 4;
std::cout << elements_1[0] << std::endl;
```

A kimenet így **4** lesz.

## std::vector

Eddig csak olyan tömbökről volt szó, amiknek ismert a mérete futási időben.
Gyakori igény azonban, hogy a méret futási időben változhasson.
C-ben erre a problémára a `malloc` és `free` függvényeket használtuk.
Ez azonban nem volt túl kényelmes és sok hibalehetőséget rejtett magában.

Már egy elem tömbhöz való hozzáfüzése is rendkívül körülményes tud lenni:

```c
int *elements = malloc(3 * sizeof(int));
elements[0] = 1;
elements[1] = 2;
elements[2] = 3;
elements = realloc(elements, 4 * sizeof(int));
elements[3] = 4;
free(elements);
```

A kódunk pedig még félelmetesebbé válna ha arra is odafigyelnénk, hogy sikerültek-e a memóriafoglalások.

Az `std::vector` azért létezik, hogy jelentősen megkönnyítse a fejlesztők dolgát amikor tömbökkel kell dolgozniuk.
Használatához szükségünk van a `<vector>` fejlécre:

```cpp
#include <vector>
```

Ezután már használhatjuk is:

```cpp
std::vector<int> elements = {1, 2, 3};
elements.push_back(4);
```

Az `std::vector`-ral nem kell foglalkoznunk a memóriafoglalással és felszabadítással.
Ha nem sikerül a memóriafoglalás, akkor kivételt dob.
Amikor pedig kikerül a kontextusából, akkor a destruktora automatikusan felszabadítja a memóriát.

Az `std::array`-hoz hasonlóan az `std::vector` is tud érték szerinti másolást:

```cpp
std::vector<int> elements_1 = {1, 2, 3};
std::vector<int> elements_2 = elements_1;
elements_2[0] = 4;
std::cout << elements_1[0] << std::endl;
```

A kimenet **1**.

## std::span

Az előzőek után felmerül a kérdés, hogy miképp veszünk át paraméterként egy tömböt?

Kézenfekvő megoldás lehet, hogy ugyanúgy csináljuk mint eddig C-ben.

```cpp
void print_array(int *elements, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << elements[i] << std::endl;
    }
}
```

Az átadás viszont nem megy triviálisan:

```cpp
std::array<int, 3> elements = {1, 2, 3};
print_array(elements, 3); // Ez a kód nem fordul!
```

Ha kicsit okoskoduk, akkor azért működésre lehet bírni:

```cpp
print_array(&elements[0], 3);
```

Habár a fenti működik, azért még mindig vannak vele problémák.
Tegyük fel, hogy átméretezem a tömbömet, hogy most már csak kettő elemet tartalmazzon.
Ebben az esetben ha elfelejtem a függvényhívásnál is átírni a méretet, akkor máris helytelenül működik a kódom.

Utánaolvasva az `std::array` dokumentációjának, akkor a következő kód már megoldja ezt a problémánkat is:

```cpp
print_array(elements.data(), elements.size());
```

Érezhető azonban, hogy ez még mindig nem egy tökéletes megoldás.
Sokkal kézenfekvőbb lenne ha egy tömböt önnmagában tudnánk átadni paraméterként és nem kéne szórakozni az első elemére mutató pointerrel meg a méretével.
Erre megoldást nyújt egy C++ nyelvi elem, a **referencia**:

```cpp
void print_array(std::array<int, 3> &elements) {
    for (size_t i = 0; i < 3; ++i) {
        std::cout << elements[i] << std::endl;
    }
}
```

Ezután már szépen hívható a függvényünk az alábbi módon:

```cpp
std::array<int, 3> elements = {1, 2, 3};
print_array(elements);
```

Azzal viszont, hogy most megoldottunk egy problémát, bevezettünk egy újabbat.
Jelenleg a függvényünket kizárólag a három méretű tömbökkel lehet csak meghívni.
Ez nem pont az amire nekünk van szükségünk.
Ez is orvosolható egy C++ nyelvi elemmel, a **template**-ek használatával:

```cpp
template <size_t SIZE>
void print_array(std::array<int, SIZE> &elements) {
    for (int i = 0; i < SIZE; ++i) {
        std::cout << elements[i] << std::endl;
    }
}
```

Habár elsőre ez nagyon jó megoldásnak tűnik, azért nem árt tudni, hogy elkövettünk itt egy elég nagy galádságot.
A fordító a `print_array` függvényünkből annyi darabot fog készíteni ahány különböző mérettel szeretnénk használni.
Ez feleslegesen növeli az elkészült bináris méretét.

Arról pedig ne is beszéljünk, hogy a függvényünk nem hívható meg `std::vector`-ral.
C-ben eddig megírt függvényeinknek nem számított, hogy egy konstans méretű vagy átméretezhető tömböt kaptak paraméterként.
Csak végigiteráltak az elemeken és kiírták őket.
Erről semmiképp nem szeretnénk lemondani.

Látható, hogy hiába próbáltunk a nyelvi elemekkel okoskodni, nem igazán jutottunk használható megoldáshoz.
Viszont C++20-tól kezdődően létezik egy `std::span` nevű osztály, ami minden eddigi problémánkat megoldja.
Használatához szükségünk van a `<span>` fejlécre:

```cpp
#include <span>
```

Ezután már használhatjuk is:

```cpp
void print_array(std::span<int> elements) {
    for (size_t i = 0; i < elements.size(); ++i) {
        std::cout << elements[i] << std::endl;
    }
}
```

A függvény hívása is magától értetődő:

```cpp
std::array<int, 3> stack_elements = {1, 2, 3};
print_array(stack_elements);

std::vector<int> heap_elements = {4, 5, 6, 7};
print_array(heap_elements);
```

Nagyon jó, hogy ez a megoldás működik.
De azt fontos megérteni, hogy mi történik a háttérben.
Az `std::span` osztály belül ugyanúgy tartalmaz egy pointert és egy méretet.
Csupán egy absztrakciós réteget nyújt, hogy ne nekünk kelljen vele bajlódni (és ezáltal potenciálisan elrontani).

## Összefoglalás

A C-s tömbök használatával csak a gond van.
Ha C++ kódot írunk és tömbre van szükségünk, akkor:

- Amikor létrehozzuk a tömböt akkor át kell gondolnunk, hogy ismert-e a mérete fordítási időben?
    - Igen: használjunk `std::array`-t.
    - Nem: használjunk: `std::vector`-t.
- Ha pedig függvény paraméterként szeretnénk átadni egy tömböt és nem szeretnénk másolatot, akkor minden esetben használjunk `std::span`-t.

## Gyakran ismételt kérdések

01. Tudom, hogy egy függvényemet csak `std::vector`-ral szeretném hívni, vehetek-e át `const std::vector &`-t?

    Működni működik, de nem jó ötlet.
    A referencia igazából egy pointer, tehát egy elem elérése esetén először követnünk kell egy pointert az eredeti `std::vector`-ra, majd abban lévő pointer segítségével már elérhetjük az elemet.
    Ez kettő indirekciót jelent.

    Az `std::span`-ben lévő pointer viszont kapásból az adatra mutat, tehát egy indirekcióval kevesebbre van szükség.
