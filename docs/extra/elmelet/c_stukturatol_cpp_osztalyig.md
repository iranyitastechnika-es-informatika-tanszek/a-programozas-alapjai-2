# C struktúrától C++ osztályig

Emlékezzünk vissza a Prog1-ből a [DinamikusTömb](https://infoc.eet.bme.hu/ea07/#12)-re.

Ebben az írásban azt fogjuk megnézni, hogy hogyan lehet ezt a struktúrát és a hozzá tartozó függvényeket egy C++ osztállyá alakítani.

## DinamikusTömb struktúra

=== "C"

    C-ben szükséges a `typedef` kulcsszó, hogy a struktúrát ne kelljen mindig `struct`-ként hivatkozni.

    ```c
    typedef struct DinTomb {
        double *adat;
        int meret;
    } DinTomb;
    ```

=== "C++"

    Két dolog változott:

    - `struct` helyett `class`-t használunk.
    - Nem szükséges a `typedef` kulcsszó, mert a `class`-t egyben típusdefiníciónak is tekinti a fordító.

    ```cpp
    class DinTomb {
        double *adat;
        int meret;
    };
    ```

## DinamikusTömb létrehozása

A DinamikusTömb függvényei közül az első a Dinamikus tömböt létrehozó függvény.

=== "C korábban"

    Prog1-ből az alábbi volt.

    ```c
    typedef struct DinTomb {
        double *adat;
        int meret;
    } DinTomb;

    bool dintomb_foglal(DinTomb *dt, int meret) {
        dt->meret = meret;
        dt->adat = (double *)malloc(meret * sizeof(double));
        return dt->adat != NULL;
    }
    ```

=== "C átírva"

    Végezzünk néhány változtatást!

    - A méret típusa legyen `size_t`, az pont erre való.
    - Nevezzük át a függvényünket és paramétereit.
    - Használjunk [designated initializer](https://gcc.gnu.org/onlinedocs/gcc/Designated-Inits.html)t.
    - Kezeljük másképp a hibát.
    - Illetve egészétsük ki azzal a plusz funkcionalítással, hogy a frissen foglalt tömb elemeit 0-ra inicializáljuk.

    ```c
    typedef struct DinTomb {
        double *adat;
        size_t meret;
    } DinTomb;

    void DinTomb_construct(DinTomb *this, size_t meret) {
        *this = (DinTomb){
            .meret = meret,
            .adat = (double *)malloc(meret * sizeof(double))};
        if (this->adat == NULL) {
            printf("Nem sikerült memóriát foglalni!\n");
            exit(1);
        }
        for (size_t i = 0; i < meret; i++) {
            this->adat[i] = 0.0;
        }
    }
    ```

=== "C++"

    Íme ugyanez C++ nyelven. Vegyük sorra a különbségeket!

    - A inicializálást végző függvény nem a levegőben lóg, hanem az osztály része.
    - Ennek neve szigorúan megegyezik az osztály nevével.
    - Paraméterként nem kell átadni az osztályt (`DinTomb *this`), mert az híváskor implicit átadódik.
    - A designated initializer helyett inicializáló lista van.
    - `malloc` helyett `new`-t használunk memóriafoglaláshoz.
    - Nem kell ellenőrizni, hogy sikerült-e a memóriafoglalás (hiba esetén kivételt dob a `new`).
    - A `for` ciklusban nem szükséges `this->adat`-ot írni, elég az `adat` is.

    ```cpp
    class DinTomb {
        double *adat;
        size_t meret;

        DinTomb(size_t meret)
            : meret{meret}
            , adat{new double[meret]} {
            for (size_t i = 0; i < meret; i++) {
                adat[i] = 0.0;
            }
        }
    };
    ```

## DinamikusTömb felszabadítása

!!! warning "Figyelem"

    Ez az oldal még nem készült el.

## DinamikusTömb metódusai

!!! warning "Figyelem"

    Ez az oldal még nem készült el.
