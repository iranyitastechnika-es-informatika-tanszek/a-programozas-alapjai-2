# Kivételkezelés

A kivételkezelés C++-ban tipikusan egy kényes téma, mivel gyakran nem egyértelmű, hogyan kéne helyesen csinálni.
Ennek ellenére összeszedtünk számotokra pár alapelvet, amelyek mentén haladva könnyen érthetővé és helyessé formálhatjátok a kódotokat.

## Bevezetés

### Függvények interfésze

A mostani megközelítésben minden függvényt a következő részekre fogjuk bontani:

- A függvény feladata
- Bemenetek
- Kimenetek
- Kivételek
- [Szerződések](contracts.md)

???+ example "Példaként vegyük az alábbi függvényt, hogy könnyű legyen megérteni"

    ```cpp
    // this code smells, we'll explain why later
    int to_int(const std::string& string);
    ```

    A függvény feladata string-ek számmá konvertálása a számunkra jól ismert módon ("3" -> 3).

    A bemeneteket és a kimeneteket igyekezünk úgy megformálni, hogy azok minél jobban egyezzenek a függvény feladatában leírtakkal.
    Jelen példában ehhez az `std::string`-et használjuk, mert ezzel tudjuk lemodellezni a lehető legkényelmesebbenn a bemenetet.
    Visszatérési értékhez `int`-et használunk.

    A kivételek alatt a függvény által potenciálisan dobandó kivételeket értjük.
    Arra majd később térünk ki pontosabban, hogy mit érdemes dobni.

    Végül a függvény szerződésében leírjuk, hogy a kapott string egy egész számot szeretnénk, hogy reprezentáljon, és a visszatérési érték is ez a szám lesz.

### A probléma

A probléma nagyon egyszerű.
A modellezni kívánt környezet nem írható le ideálisan programozási nyelven.
És, a hardver amit használunk sem ideális.

### Bug-ok és kivételek

Elsőnek is vegyük észre, hogy a függvény előfeltételének teljesülése nem minden esetben elég annak sikeres lefutásához.

!!! info "Az előfeltétel nem teljesülése"

    A függvény előfeltételeinek nem teljesülését nem tekintjük kivételnek.
    A függvény nem vállal felelősséget ez esetben a helyes működéséért.
    Így ezt az esetet, és minden más esetet amikor egy szerződés nem teljesül, **bug**-nak tekintünk.

Amikor a függvény az előfeltételeinek teljesülésének ellenére nem képes helyesen működni, akkor azt mondjuk, hogy kivétel történt.
Ekkor két eshetőséggel kell, hogy szembenézzünk:

1.  A kivétel már biztosan nem kezelhető.
    A kódunk teljesen rossz útra tért, már egyáltalán sehogy sem tudjuk megmenteni.
    Legjobb, ha helyben felhagyunk minden reménnyel és leállítjuk a programot.

2.  Habár kivételbe ütköztünk, még menthető a helyzet.
    Próbáljuk meg kezelni a kivételt.

!!! danger "Logikai/programozói kivételek"

    A logikai hibákat nem tekintjük kezelhető kivételnek, hiszen eredetüket sem ismerjük.
    Ha a kódban [`std::logic_error`](https://en.cppreference.com/w/cpp/error/logic_error)-t vagy ahhoz hasonlót találunk, akkor ott valami bűzlik.
    Gyakran a bug-ok jelzésére használják ezt a kivételtípust, azonban ez nem helyes.
    Ha bug-gal találkozunk, ne kezelni, hanem kiírtani próbáljuk azt.
    A bug-ok megtalálásához pedig teszteljünk!

## Kivételek kezelése

### std::optional

A kivételkezelés legprimitívebb módja, amikor helyben kezeljük a kivételt.

!!! example ""

    ```cpp
    int to_int(const std::string& string) {
        // ...

        // An exception happened, we are no longer able to produce the desired result.
        // What to do?

        // ...
    }
    ```

Mivel a kívánt értékkel nem tudunk visszatérni, valami más módot kell kitalálni a visszatérésre.

!!! bug "Cifra visszatérési értékek"

    A függvény kívánt visszatérési típusa adott. Ennek ellenére mi nem használhatjuk azt.
    Ha egy `int`-tel visszatérő függvény kivétel esetén `-1`-gyel, vagy valami hasonló hibakóddal tér vissza, akkor a hívó nem fogja tudni megkülönböztetni a helyes visszatérési értéket a hibakódtól. Ezért **sose térjünk vissza ugyan olyan típussal, mint amit egyébként a függvény visszaadna**.

Még mielőtt bármilyen érdekes próbálkozásra vetemednénk, leleplezzük a megoldást:
Változtassuk meg a függvény visszatérési típusát.
Innentől legyen képes kétféle állapotot is tárolni - amikor sikerült a kívánt értéket létrehozni, és amikor nem.

Az [`std::optional`](https://en.cppreference.com/w/cpp/utility/optional) pontosan ezt biztosítja számunkra.
Egy olyan szabványos tároló, amiben vagy van egy elem, vagy nincs.

???+ example "A példakódunk ezután így néz ki"

    ```cpp
    std::optional<int> to_int(const std::string& string);
    ```

### std::expected

Általában a kivételeket nem érdemes helyben kezelni.
A függvényen kívül könnyebb eldönteni, hogyan folytassa hiba esetén a működését a program.

!!! note inline end ""

    Az `std::expected` mindig vagy egy elvárt értéket tárol, vagy egy másik nem elvárt elemet.

A kivételre akár tekinthetünk úgy is, mint egy alternatív visszatérési értékre.
Erre a célra született az [`std::expected`](https://en.cppreference.com/w/cpp/utility/expected) szabványos tároló, ami a kívánt érték helyett egy másféle értéket is képes tárolni.

!!! example "Mikor használjunk `std::expected`-et?"

    `std::string` `int`-té konvertálásánál gyakori, hogy a megadott string-ben nem egy szám szerepel; vagy ami benne szerepel szám, az nem `int`-té konvertálható.
    Ezeknél az egyértelműen nem kivételektől mentes függvényeknél használjunk `std::expected`-et a kivétel kezelésére.

    ```cpp
    enum class Exception {
        not_a_number,
        not_an_int,
        number_is_too_big
    }

    std::expected<int, Exception> to_int(const std::string& string);

    // ...

    std::expected<int, Exception> result = to_int("maybe a number");

    if (!result.has_value()) {
        // treat exception
    }
    ```

!!! note "`void` visszatérésű függvények kivételkezelése"

    Az egyébként visszatérési értékkel nem rendelkező függvényeknél is célszerű `std::expected<void, Exception>`-t használni.
    Ez sokkal szebb, mintha egy `std::optional<Exception>`-nel térnénk vissza.

### Kivételdobás

A fent említett megoldásokkal már teljes a kivételkezelés.
Azonban pár valóban kivételes kivétel esetében létezik egy másik út is, a kivételdobás.

Egy pillanatra képzeljük el, hogy egy komplex, több folyamatból álló rendszeren dolgozunk.
Mivel mi vagyunk a felelősek a kód bázis több részéért is, sok információval rendelkezünk, amivel egy egyébként kisebb modul feldolgozásánál nem rendelkeznénk.
A kódunk egy részében egy új mély folyamatot szeretnénk elvégezni, és a feladatunk az új folyamat kódjának megírása.
Az új folyamat is, mint a rendszerünk, viszonylag bonyolult, függvények sorozatából áll.
A folyamat futása közben az egyik függvényben kivétel keletkezik, ami számunkra különösen érdekes, ugyanis tudjuk, hogy ebben a kivételes esetben már az egész folyamat nem folytatható tovább.
Megtehetnénk, hogy minden egyes függvényben egyesével felkészülünk erre az eshetőségre. Ám, ha több ilyen kivétel is keletkezhet, akkor könnyen látjuk, hogy csak emiatt a kódbázisunk egy viszonylag nagy részét nem érdemes mindig újraírni.

Az ilyen esetekben érdemes a kivételt *eldobni*.

!!! danger ""

    Kivételeket nem szoktunk csak úgy dobni a kódban.
    Egyrészt azért, mert a kivételkezelési `throw & try-catch` mechanizmusnak nagy a futási idejű költsége, **ha egy kivétel eldobódik**.
    Másrészt pedig azért, mert nehéz végig követni, hogy mikor éppen milyen kivétel utazhat a rendszerben.

## Bónusz

### Ajánlás

A kivételkezelés általánosságban nem egy egyszerű probléma.
Így aki bővebben utána szeretne járni a modern *best practice*-eknek, azoknak [ezt](https://youtu.be/HXJmrMnnDYQ?si=boMyPXNhPh6HYo4X) a videót ajánljuk.

### `noexcept`

C++ támogatja a függvények `noexcept`-nek jelölését, ami azt jelenti, hogy az adott függvény semmilyen körülmények között nem fog kivételt dobni.
A `noexcept`, mint nyelvi elem, kifejezetten azzal a céllal került a szabványba, hogy néhány esetben az STL optimalizálni tudjon ez alapján.
Nem arra van, hogy minden függvényt, ami nem dobna, `noexcept`-tel jelöljünk!
Hogy miért, azt lásd bővebben itt: [*The Lakos Rule*](https://quuxplusone.github.io/blog/2018/04/25/the-lakos-rule/).
