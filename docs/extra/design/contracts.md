# Szerződések *(Contracts)*

"Design by Contract (DbC)" - egy híres objektum orientált tervezési elv, amely még ma is hatalmas jelentőséggel bír.
Nem tesz mást csak annyit, hogy - egy függvény vagy egy osztály valóban azt csinálja, mint ami neki a dolga.

## A szerződések

Minden tagfüggvény leírható háromféle feltétel kihasználásával:

1.  **Előfeltételek *(preconditions)***

    A függvényhíváskor elvárt igazságok teljesülését írják elő.

    Például:

    -   Osztásnál elvárjuk, hogy az osztó ne legyen nulla.

    -   C-s [strcat](https://en.cppreference.com/w/cpp/string/byte/strcat):

        ```cpp
        char str[50] = "Hello ";
        char str2[50] = "World!";

        strcat(str, str2);
        strcat(str, " Goodbye World!");
        ```

        ???+ quote "Részlet a dokumentációból"
            
            ```cpp
            char* strcat( char* dest, const char* src );
            ```

            Parameters:

            - **dest** - pointer to the null-terminated byte string to append to
            - **src**  - pointer to the null-terminated byte string to copy from 

            The behavior is undefined if the destination array is not large enough for the contents of both `src` and `dest` and the terminating null character.

            The behavior is undefined if the strings overlap.

    -   Egy tároló objektumból való elem eltávolításakor elvárjuk, hogy legyen a tárolóban elem.

        ```cpp
        std::vector<int> storage;
        storage.pop_back();  // precondition violation 
        ```

2.  **Utófeltételek *(postconditions)***

    A függvényhívás végén érvényben levő igazságok.

    Például:

    -   Egy tároló objektumhoz hozzáadáskor elvárjuk, hogy a művelet végén eggyel több elem legyen a tárolóban.

    -   Egy string-el visszatérő függvénynél elvárhatjuk, hogy ne üres string-et kapjunk.

        ```cpp
        int number = 4;
        std::string representation = std::to_string(number);
        assert(representation.empty() == false && "Postcondition violated");
        ```

3.  **Invariánsok *(invariants)***

    Az objektumok belső állapota érvényes marad függvényhívás után is.

    Például:

    -   Egy tároló által lefoglalt terület mindig legalább akkora, hogy a jelenleg tárolt elemek abban elférjenek.

        ```cpp
        std::vector<int> storage;
        storage.push_back(13);
        assert(storage.capacity() >= storage.size() && "Invariant violated");
        ```

    -   Pozíció koordináták tárolásakor elvárjuk, hogy azok érvényesek legyenek.

        ```cpp
        struct Position {
            int x;  // always should be between 0 and 1920
            int y;  // always should be between 0 and 1080
        };// (1)!
        ```

        1.  Ilyen formában nagyon nehéz betartani az invariánsokat.
            Érdemes x-et és y-t elrejteni privát változóként, és azokat csak setter-ekkel állítani, hogy az invariáns mindig érvényes maradjon.

## Általános felhasználás

Függvényíráskor az első lépés mindig a függvényhez kapcsolódó szerződések leírása kell, hogy legyen.
Erre a ma használt általános megoldás C++-ban a C-s `assert` és ennek alternatív definiálásai.

Az előfeltételek mindig a függvénydefiníció legelejére kerülnek.

```cpp
int foo(int x) {
    assert(x > 3);
    // ...
}
```

Az utófeltételek helye a függvénydefiníció végén kéne, hogy legyen.

```cpp
int bar(int y) {
    int result;
    // ...
    assert(result < 1);
    return result;
}
```

Az invariánsok ellenőrzése pedig megoldható elő- és utófeltételek kombinálásával.

```cpp
void Foo::baz(int z) {
    assert(this->a == 0)
    // ...
    assert(this->b >= this->c);
}
```

Ez így mind elég csúnya kódban, ezért nem is szokták mindig mindet kiírni.
Tipikusan a függvény dokumentációjába kerülnek annak szerződései emberi nyelven megfogalmazva.
Pl.:

```cpp
// Hozzáfűz egy elemet a tárolóhoz
void push_back(int i);
```

## Típusbiztonság

Nagyon sok szerződés megírása elkerülhető helyes típushasználattal.
Például, ha a függvényünk csak nemnegatív egész számokon értelmezett, akkor `int` helyett használjunk `unsigned int`-et.

### A New Type idióma

Sokszor a beépített típus rendszer sem biztosít elegendő korlátozást a függvényeink számára.
Ilyenkor érdemes lehet egy új típus bevezetése.

Tegyük fel, hogy a függvény amit írtunk, csak email formátumú string-ekkel működik.

```cpp
void notify(const std::string& email);
```

Ekkor megtehetjük egy új Email típus bevezetését, ami garantálja, hogy ennek példányai mind helyes formátumúak.
Így a függvényünknél már nem kell törődni az átvett érték helyességével, mert ezt a típusrendszer biztosítja.

```cpp
void notify(const Email& email);
```

!!! tip "Bónusz: szerződések tesztelése"

    A kódban megírt szerződéseket is lehet (és kell is) tesztelni.
    Ehhez az alábbi [linken](https://www.youtube.com/watch?v=jTuIzSUxYDc) található videót ajánlom.
