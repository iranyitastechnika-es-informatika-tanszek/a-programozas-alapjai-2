# CLion

A [CLion](https://www.jetbrains.com/clion/) egy [JetBrains](https://www.jetbrains.com/) által fejlesztett prémium cross-platform C/C++ IDE.

*[IDE]: Integrated Development Environment

A VSCode-hoz hasonlóan ez is plugin-okkal működik, amelyek közül sok már alapból integrálva van.
Illetve beépített C++ fordítóval, CMake-kel, és egyéb eszközökkel van felszerelve.

## UI

A grafikus felület több eszköztárból áll, amelyek az ablak négy oldalán helyezkednek el.
A **Beállítások** menüpont például a bal felső sarokban a hamburger menü alatt érhető el.

## Fordítás és futtatás

A CLion beépítetten [CMake](https://cmake.org/)-et és [Meson](https://mesonbuild.com/)-t is támogat.
Ebben a cikkben a CMake-ről lesz szó.

### Toolchains

Új projekt megnyitásakor felugrik egy ablak, ami kéri, hogy a kívánt fordításhoz szükséges eszközöket legyünk szívesek beállítani.
Egyéb esetben a beállításokon belül a `Build, Execution, Deployment > Toolchains` menüpontra kattintva érhető el ez az ablak. 
Itt a `+` gombbal tudunk felvenni új toolchain-t, amivel beállíthatjuk a fordításhoz szükséges eszközöket.

!!! tip "Windows-on MSYS2-t használva a MinGW opciót kell mint Toolchain kiválasztani, attól függetlenül, hogy ezen belül MingW környezetet használunk-e."

### CMake

Ezután hozzunk létre egy CMake profilt a `Build, Execution, Deployment > CMake` alatt.
Itt válasszuk ki az előbb létrehozott toolchain-t.
Ha szeretnénk, még egyéb tényezőket is megadhatunk, például milyen mappába kerüljön a fordítás kimenete.

A CMake-es projektünk [konfigurálása](../fejlesztoeszkozok/cmake.md#konfigurálás) általában magától lezajlik a háttérben.
Egyéb esetben mennyünk biztosra, hogy a projekt gyökerében megtalálható a `CMakeLists.txt`, és ez helyes leírást tartalmaz.
Manuálisan konfigurálni bal oldalt a CMake ikonra kattintva, a 🔄 ikonnal lehet;
vagy bal fent a hamburger menü alatt a `🔄 Reload CMake project`-tel.

### Fordítás és futtatás

A felső eszköztár közepén érhetőek el a fordítás, futtatás, debugolás gyors opciók.

![Build, run, debug](../../media/Clion_build-run-debug.png)

Itt kiválaszthatjuk, hogy milyen CMake profillal szeretnénk dolgozni, és melyik CMake target-en.

### Git

A CLion remek git grafikus felületet biztosít.
Commit-oláshoz elérhető a bal oldali eszköztár felső részében a `Commit` ablak, illetve a git repó grafikus megtekintéséhez és szerkesztéséhez a bal alsó sarokban a `Git` ablak.
