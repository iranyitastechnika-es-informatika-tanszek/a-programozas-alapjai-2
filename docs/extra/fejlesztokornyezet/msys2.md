# MSYS2

!!! note inline end ""
    
    A Clang-et Visual Studio Installer-el is telepíthetünk, de ezzel csak a Visual Studio-hoz tartozó C++-os standard könyvtárat leszünk képesek használni.

Az [MSYS2](https://www.msys2.org/) igyekszik egy Linux-hoz hasonló fejlesztői környezetet biztosítani Windows alá, építve a [Cygwin](https://cygwin.com/) alapjaira.
Segítségével Windows-on is elérhetővé válnak többek között a [GCC](https://gcc.gnu.org/) és [Clang](https://clang.llvm.org/) modern C/C++ fordítók.

## Telepítés

A telepítés során érdemes a [weboldal leírását követni](https://www.msys2.org/#installation).
Ez viszonylag egyszerű és tömör.
Ám még mielőtt nagy hévvel mindent letelepítenénk, ajánlom a [következő fejezet](#környezetek) megismerését.

## Környezetek

MSYS2 alatt [többféle környezetet](https://www.msys2.org/docs/environments/) is használhatunk (ucrt64, clang64, mingw64, ...).
Telepítés előtt érdemes megérteni a különbségeket.

Alapvetően ajánlott az `UCRT64`.
Ez *x86_64*-es architektúrát használ, és alapértelmezett fordítója a **GCC**, amelyhez tartozó standard C++ könyvtár a *libstdc++*.
Clang-gel is lehet fordítani, de ebben az esetben a GNU-s (GCC-s) linker és standard könyvtár lesz használva.

**LLVM/Clang** alapú toolchain használatához, ajánlott a `CLANG64` nevű környezet.
Itt a Clang fordító és a hozzá tartozó *libc++* C++ könyvtár az alapértelmezett.

Régen a `MINGW64` környezet volt az ajánlott, ami szintén GCC-t használ, de az ehhez tartozó C-s könyvtár, az *MSVCRT*, már elavult.
Ajánlott helyette az *UCRT* használata, például a `UCRT64` vagy a `CLANG64` környezet alatt.

## VSCode integrált terminál

Az MSYS2-höz alapértelmezetten saját terminál is tartozik.
VSCode-ban lehetőségünk van ennek elérésére és [használatára integrált terminálként](https://code.visualstudio.com/docs/terminal/profiles).
Ehhez vagy a felhasználói szintű `settings.json` fájlt kell szerkeszteni(1), vagy a jelenlegi workspace alá tartozót, ami a `.vscode` mappa alatt kell, hogy legyen(2).
{ .annotate }

1.  ++ctrl+shift+p++ -> *Preferences: Open User Settings (JSON)*
2.  ++ctrl+shift+p++ -> *Preferences: Open Workspace Settings (JSON)*

```json title="settings.json"
{
    //...
    "terminal.integrated.profiles.windows": {
        "MSYS2": {
            "path": "C:/msys64/usr/bin/bash.exe",//(1)!
            "args": [
                "--login",//(2)!
                "-i"//(3)!
            ],
            "env": {
                "MSYSTEM": "UCRT64",//(4)!
                "CHERE_INVOKING": "1"//(5)!
            }
        }
    },
}
```
{ .annotate }

1.  Az MSYS2 alatt lévő `bash.exe` elérési útvonala
2.  Megnyitás login shell-ként
3.  Megnyitás [interaktív shell](https://www.gnu.org/software/bash/manual/html_node/What-is-an-Interactive-Shell_003f.html)-ként
4.  A használni kívánt MSYS2 környezet
5.  A jelenleg használt working directory-ba lépjünk be

## Package Management

!!! note inline end ""

    A `pacman` alapvetően az [Arch Linux](https://archlinux.org/) csomagkezelő rendszere.
    Amit az MSYS2 használ, az ennek csak egy változata.

Az MSYS2 `pacman`-t használ, mint csomagkezelő.
A telepíteni kívánt szoftvereket is ezen keresztül kell letölteni.
Bővebb információ a package manager használatáról megtalálható [itt](https://www.msys2.org/docs/package-management/), az elérhető csomagok listája pedig [itt](https://packages.msys2.org/package/).
A legtöbb csomag telepítéséhez elegendő a hozzá tartozó webes leírásban lévő telepítő parancs kimásolása és futtatása egy MSYS2 terminálból.
