# Visual Studio Code

A korábbiakban számos fejlesztést segítő eszközről volt szó, mint például a CMake vagy a Clang-Format, viszont eddig mindegyiknek csak a parancssoros interfészét mutattuk be.
Most azt fogjuk áttekinteni, hogy miként lehet ezeket integrálni a Visual Studio Code-ba, hogy minél kényelmesebben tudjunk dolgozni.

## CMake

A CMake-et a [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) nevű bővítménnyel tudjuk használni.

Telepítés után a **CMakeLists.txt** fájl színezésén kívül változás még az, hogy kaptunk egy csomó új parancsot.
A parancsokat a **Help > Show All Commands**(1) menüpont alatt találjuk meg.
{ .annotate }

1.  ++ctrl+shift+p++

Kezdjük el gépelni, hogy **CMake: Show**.
Két kiegészítést kell, hogy lássunk:

- **CMake: Show Configure Command**
- **CMake: Show Build Command**

Próbáljuk ki mindkettőt, hogy lássuk, hogy milyen parancsokat futtat a CMake a háttérben.
Hasonlítsuk össze azzal amit [korábban](../fejlesztoeszkozok/cmake.md/#cmake_1) a parancssorban futtattunk!

Ezután nyugodtan próbáljuk ki a **CMake: Configure** és **CMake: Build**(1) parancsokat is.
{ .annotate }

1.  Ha még nem futtattuk le a **CMake: Configure** parancsot, akkor a **CMake: Build** parancs ezt automatikusan meg fogja tenni.

Végül jön a legfontosabb parancs, a **CMake: Debug**(1).
Ehhez még először szükség van a [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) bővítményre.
Utána már simán kell, hogy működjön minden.
{ .annotate }

1. Szintén magától futtatja a **CMake: Build** parancsot, ha még nem futtattuk le.

Próbáljuk ki, hogy megáll-e a kód egy breakpointnál!

!!! note

    A CMake Tools bővítmény segítségével futtatott program munkakönyvtára a **build** könyvtár lesz.
    Ez kellemetlen meglepetésekhez vezethet, ha a programunk fájlokat próbál írni vagy olvasni.
    Szerencsére viszont a **.vscode/settings.json** fájlba beállíthatjuk a munkakönyvtárat.

    ```json title=".vscode/settings.json"
    {
        "cmake.debugConfig": {
            "cwd": "${workspaceFolder}"
        }
    }
    ```

## Clang-Format

A Clang-Formatot a [clangd](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd) nevű bővítménnyel tudjuk használni.
Innentől kezdve a **.clang-format** fájlban lévő beállításokat fogja használni a Visual Studio Code.
A formázást Linuxon a ++ctrl+shift+i++, Windowson az ++alt+shift+f++, Mac-en a ++shift+option+f++ billentyűkombinációval tudjuk megtenni.

Még jobban járunk azonban, ha a **File > Preferences > Settings** menüpontban a **Editor: Format On Save** beállítást bekapcsoljuk.
Vagy ha csak az aktuális projektben szeretnénk használni, akkor a **.vscode/settings.json** fájlba írjuk be a következőt:

```json title=".vscode/settings.json"
{
    "editor.formatOnSave": true
}
```
