# A Programozás alapjai 2

Ez egy nem hivatalos tárgyhonlap!
Az eredeti megtalálható [itt](https://infocpp.iit.bme.hu/).

Abból a célból készítettem ezt az oldalt, hogy a laborra kiadott feladatok könnyebben emészthetőek legyenek.
Tehát másolhatóak a kódrészletek és van rendes reszponzív megjelenés meg sötét téma is.
